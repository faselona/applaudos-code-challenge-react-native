import React from "react";
import { StatusBar } from "react-native";
import { Provider } from "react-redux";
import {
  Provider as PaperProvider,
  DarkTheme as PaperDarkTheme,
} from "react-native-paper";
import {
  NavigationContainer,
  DarkTheme as NavigationDarkTheme,
} from "@react-navigation/native";

import Navigation from "./src/navigation/Navigation";
import { brightTurquoise, blueDianne, mirage } from "./src/utils/colors";

import createStore from "./store";

const { store: storeInstance } = createStore();
export const store = storeInstance;

export default function App() {
  PaperDarkTheme.colors.primary = brightTurquoise;
  PaperDarkTheme.colors.accent = brightTurquoise;

  NavigationDarkTheme.colors.background = blueDianne;
  NavigationDarkTheme.colors.card = mirage;
  return (
    <Provider store={store}>
      <PaperProvider theme={PaperDarkTheme}>
        <StatusBar barStyle={"light-content"} backgroundColor="#15212b" />
        <NavigationContainer theme={NavigationDarkTheme}>
          <Navigation />
        </NavigationContainer>
      </PaperProvider>
    </Provider>
  );
}
