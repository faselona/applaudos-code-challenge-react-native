import axios from "axios";
import qs from "qs";

import { API_URL } from "../utils/constants";

export const URLEncoded = {
  "Content-Type": "application/x-www-form-urlencoded",
};

export const Multipart = {
  "Content-Type": "multipart/form-data",
};

export const JSONBody = {
  "Content-Type": "application/vnd.api+json",
};

export const RequestType = {
  URLEncoded,
  Multipart,
  JSONBody,
};

export const Methods = {
  HEAD: "head",
  GET: "get",
  POST: "post",
  PUT: "put",
  PATCH: "patch",
  DELETE: "delete",
};

export const getOptions = ({
  data,
  method,
  headers: extraHeaders = {},
  params = {},
}) => {
  const headers = {
    Accept: "application/vnd.api+json",
    ...JSONBody,
    ...extraHeaders,
  };

  const options = {
    baseURL: API_URL,
    method,
    data,
    headers,
    params,
    paramsSerializer: (params) => {
      return qs.stringify(params, { encode: false });
    },
  };

  return options;
};

export default (url, options) => axios({ url, ...getOptions(options) });
