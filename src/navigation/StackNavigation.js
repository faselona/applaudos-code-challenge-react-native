import React from "react";
import { IconButton } from "react-native-paper";
import { createStackNavigator } from "@react-navigation/stack";

//Screens
import Home from "../screens/Home";
import Detail from "../screens/Detail";
import Search from "../screens/Search";

const Stack = createStackNavigator();

export default function StackNavigation({ navigation }) {
  const menuButton = (screen) => {
    switch (screen) {
      case "home":
        <IconButton icon="menu" onPress={() => navigation.openDrawer()} />;
        break;

      default:
        return (
          <IconButton icon="arrow-left" onPress={() => navigation.goBack()} />
        );
        break;
    }
  };

  const searchButton = () => {
    return (
      <IconButton
        icon="magnify"
        onPress={() => navigation.navigate("search")}
      />
    );
  };
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={Home}
        options={{
          title: "KitsuApp",
          headerLeft: () => menuButton("home"),
          headerRight: () => searchButton(),
        }}
      />
      <Stack.Screen
        name="detail"
        component={Detail}
        options={{
          title: "Serie detail",
          headerLeft: () => menuButton("detail"),
          headerRight: () => searchButton(),
        }}
      />
      <Stack.Screen
        name="search"
        component={Search}
        options={{
          title: "",
          headerLeft: () => menuButton("search"),
          headerTransparent: true,
        }}
      />
    </Stack.Navigator>
  );
}
