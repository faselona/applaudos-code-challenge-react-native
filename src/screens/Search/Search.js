import React, { useState } from "react";
import { View, ScrollView, SafeAreaView, Platform } from "react-native";
import { Searchbar } from "react-native-paper";
import { map } from "lodash";

import styles from "./styles";
import SearchItem from "../../components/SearchItem";
import Loading from "../../components/Loading";

//Custom hooks
import useFetchSearchSeries from "../../hooks/Series/useFetchSearchSeries";

const Search = ({ series, isLoading, navigation }) => {
  const [text, setText] = useState("");
  useFetchSearchSeries(text);
  return (
    <SafeAreaView>
      <Searchbar
        placeholder="Find your serie"
        iconColor={Platform.OS === "ios" && "transparent"}
        icon="arrow-left"
        style={styles.input}
        value={text}
        onChangeText={(e) => setText(e)}
      />
      <ScrollView>
        {isLoading && <Loading />}
        {!isLoading && (
          <View style={styles.content}>
            {map(series, (item, index) => (
              <SearchItem
                key={index}
                serie={item}
                onPress={() =>
                  navigation.navigate("detail", {
                    id: item.id,
                    type: item.type,
                  })
                }
              />
            ))}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Search;
