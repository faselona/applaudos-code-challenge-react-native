import { StyleSheet } from "react-native";

import { mirage } from "../../../utils/colors";

export default StyleSheet.create({
  input: {
    marginTop: -3,
    backgroundColor: mirage,
  },
  content: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
});
