import { connect } from "react-redux";
import Search from "./Search";

import { loading, data } from "../../../store/series/selectors/searchSeries";

const mapStateToProps = (state) => ({
  isLoading: loading(state),
  series: data(state),
});

export default connect(mapStateToProps)(Search);
