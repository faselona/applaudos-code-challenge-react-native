import { connect } from "react-redux";
import Home from "./Home";

import { loading, data } from "../../../store/series/selectors/anime";
import {
  loading as loadingManga,
  data as manga,
} from "../../../store/series/selectors/manga";

const mapStateToProps = (state) => ({
  isLoading: loading(state),
  anime: data(state),
  loadingManga: loadingManga(state),
  manga: manga(state),
});

export default connect(mapStateToProps)(Home);
