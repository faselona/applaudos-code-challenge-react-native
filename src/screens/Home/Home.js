import React, { useState } from "react";
import { ScrollView, View, RefreshControl } from "react-native";
import { Title } from "react-native-paper";
import { size } from "lodash";

import styles from "./styles";

//Components
import CarouselHorizontal from "../../components/CarouselHorizontal";
import Loading from "../../components/Loading";

//Custom Hooks
import useFetchAnime from "../../hooks/Series/useFetchAnime";
import useFetchManga from "../../hooks/Series/useFetchManga";

const Home = ({ isLoading, anime, loadingManga, manga, navigation }) => {
  const [animeData, setAnimeData] = useState({});
  const [mangaData, setMangaData] = useState({});
  useFetchAnime(animeData);
  useFetchManga(mangaData);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setAnimeData({ ...animeData });
    setMangaData({ ...mangaData });

    if (!isLoading) {
      setRefreshing(false);
    }
  }, [isLoading]);

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      {(isLoading || loadingManga) && <Loading />}

      {size(anime) > 0 && (!isLoading || !loadingManga) && (
        <View style={styles.series}>
          <Title style={styles.title}>Anime Series</Title>
          <CarouselHorizontal data={anime} navigation={navigation} />
        </View>
      )}
      {size(manga) > 0 && (!isLoading || !loadingManga) && (
        <View style={styles.series}>
          <Title style={styles.title}>Manga Series</Title>
          <CarouselHorizontal data={manga} navigation={navigation} />
        </View>
      )}
    </ScrollView>
  );
};

export default Home;
