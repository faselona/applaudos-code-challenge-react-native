import { connect } from "react-redux";
import Detail from "./Detail";

import {
  loading,
  data,
  genres,
} from "../../../store/series/selectors/serieDetail";

const mapStateToProps = (state) => ({
  isLoading: loading(state),
  serie: data(state),
  genres: genres(state),
});

export default connect(mapStateToProps)(Detail);
