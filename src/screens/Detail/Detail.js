import React from "react";
import { ScrollView, View, Image, SafeAreaView, Linking } from "react-native";
import { Title, IconButton } from "react-native-paper";
import { get, upperFirst, map, join } from "lodash";

import styles from "./styles";
import DetailLabel from "../../components/DetailLabel";
import Loading from "../../components/Loading";

//Hooks
import useFetchSerieDetail from "../../hooks/Series/useFetchSerieDetail";
import useFetchSerieGenres from "../../hooks/Series/useFetchSerieGenres";

const Detail = ({ route, isLoading, serie, genres }) => {
  const {
    params: { id, type },
  } = route;

  useFetchSerieDetail(id, type);
  useFetchSerieGenres(id, type);

  return (
    <SafeAreaView>
      {isLoading && <Loading />}
      {!isLoading && (
        <ScrollView style={styles.container}>
          <View style={styles.main}>
            <Image
              style={styles.mainCardImage}
              source={{
                uri: get(serie, "attributes.posterImage.small"),
              }}
            />
            <View style={styles.mainInfo}>
              <DetailLabel
                title="Main Title"
                text={
                  get(serie, "attributes.titles.en", "") ||
                  "Title not available"
                }
              />
              <DetailLabel
                title="Canonical title"
                text={get(serie, "attributes.canonicalTitle", "")}
              />
              <DetailLabel
                title="Type"
                text={`${upperFirst(get(serie, "type", ""))}, ${get(
                  serie,
                  "attributes.episodeCount",
                  ""
                )}`}
              />
              <DetailLabel
                title="Year"
                text={`${get(serie, "attributes.startDate", "")} til ${get(
                  serie,
                  "attributes.endDate",
                  ""
                )}`}
              />
            </View>
          </View>
          <DetailLabel
            title="Genres"
            text={join(
              map(genres, (genre) => get(genre, "attributes.name", "")),
              ", "
            )}
          />
          <View style={styles.body}>
            <View style={styles.infoSides}>
              <DetailLabel
                title="Average Rating"
                text={get(serie, "attributes.averageRating", "")}
              />
              <DetailLabel
                title="Age Rating"
                text={get(serie, "attributes.ageRating", "")}
              />
            </View>
            <View style={styles.infoSides}>
              <DetailLabel
                title="Episode duration"
                text={`${get(serie, "attributes.episodeLength", "")} min.`}
              />
              <DetailLabel
                title="Airing Status"
                text={upperFirst(get(serie, "attributes.status", ""))}
              />
            </View>
          </View>
          <DetailLabel
            title="Sypnosis"
            text={get(serie, "attributes.synopsis", "")}
          />
          {get(serie, "attributes.youtubeVideoId", null) && (
            <>
              <Title style={styles.trailerTitle}>
                Watch trailer on Youtube
              </Title>
              <IconButton
                style={styles.icon}
                icon="youtube"
                size={60}
                onPress={() => {
                  Linking.openURL(
                    "vnd.youtube://watch/" +
                      get(serie, "attributes.youtubeVideoId", "")
                  );
                }}
              />
            </>
          )}
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default Detail;
