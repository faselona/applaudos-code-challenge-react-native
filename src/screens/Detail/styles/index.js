import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    padding: 10,
  },
  main: {
    width: "100%",
    flexDirection: "row",
  },
  mainCardImage: {
    width: "40%",
    height: "100%",
    borderRadius: 20,
  },
  mainInfo: {
    width: "70%",
    paddingHorizontal: 9,
  },
  body: {
    width: "100%",
    flexDirection: "row",
  },
  infoSides: {
    width: "50%",
  },
  trailerTitle: {
    marginTop: 20,
  },
  icon: {
    marginBottom: 20,
  },
});
