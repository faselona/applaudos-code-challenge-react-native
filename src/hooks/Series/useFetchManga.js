import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import { assign } from "lodash";

import { getMangaRequest } from "../../../store/series/actions/manga/getManga";
const initialData = {};

const useFetchManga = (data = initialData) => {
  const dispatch = useDispatch();

  const getManga = useCallback(async () => {
    let formData = assign(data && data);

    return await new Promise((resolve, reject) => {
      dispatch(
        getMangaRequest(formData, (error, response) => {
          if (error) {
            return reject(error);
          }

          return resolve(response);
        })
      );
    });
  }, [dispatch, data]);

  useEffect(() => {
    getManga();
  }, [getManga]);
};

export default useFetchManga;
