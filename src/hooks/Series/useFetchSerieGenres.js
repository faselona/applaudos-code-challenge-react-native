import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";

import { getSerieGenresRequest } from "../../../store/series/actions/serieDetail/getSerieGenres";

const useFetchSerieGenres = (serieId, serieType) => {
  const dispatch = useDispatch();

  const getSerieGenres = useCallback(async () => {
    return await new Promise((resolve, reject) => {
      dispatch(
        getSerieGenresRequest({ serieId, serieType }, (error, response) => {
          if (error) {
            return reject(error);
          }
          return resolve(response);
        })
      );
    });
  }, [dispatch, serieId]);

  useEffect(() => {
    getSerieGenres();
  }, [getSerieGenres]);
};

export default useFetchSerieGenres;
