import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";

import { getSerieDetailRequest } from "../../../store/series/actions/serieDetail/getSerieDetail";

const useFetchSerieDetail = (serieId, serieType) => {
  const dispatch = useDispatch();

  const getSerieDetail = useCallback(async () => {
    return await new Promise((resolve, reject) => {
      dispatch(
        getSerieDetailRequest({ serieId, serieType }, (error, response) => {
          if (error) {
            return reject(error);
          }
          return resolve(response);
        })
      );
    });
  }, [dispatch, serieId]);

  useEffect(() => {
    getSerieDetail();
  }, [getSerieDetail]);
};

export default useFetchSerieDetail;
