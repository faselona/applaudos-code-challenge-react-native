import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import { assign } from "lodash";

import { getAnimeRequest } from "../../../store/series/actions/anime/getAnime";
const initialData = {};

const useFetchAnime = (data = initialData) => {
  const dispatch = useDispatch();

  const getAnime = useCallback(async () => {
    let formData = assign(data && data);

    return await new Promise((resolve, reject) => {
      dispatch(
        getAnimeRequest(formData, (error, response) => {
          if (error) {
            return reject(error);
          }

          return resolve(response);
        })
      );
    });
  }, [dispatch, data]);

  useEffect(() => {
    getAnime();
  }, [getAnime]);
};

export default useFetchAnime;
