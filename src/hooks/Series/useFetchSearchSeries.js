import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import { size } from "lodash";

import { getSearchSeriesRequest } from "../../../store/series/actions/searchSeries/getSearchSeries";

const useFetchSearchSeries = (text) => {
  const dispatch = useDispatch();

  const getSearchSeries = useCallback(async () => {
    return await new Promise((resolve, reject) => {
      dispatch(
        getSearchSeriesRequest(text, (error, response) => {
          if (error) {
            return reject(error);
          }

          return resolve(response);
        })
      );
    });
  }, [dispatch, text]);

  useEffect(() => {
    if (size(text) > 2) {
      getSearchSeries();
    }
  }, [getSearchSeries]);
};

export default useFetchSearchSeries;
