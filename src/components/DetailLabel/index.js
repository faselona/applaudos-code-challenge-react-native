import React from "react";
import { StyleSheet, View } from "react-native";
import { Text, Title } from "react-native-paper";

import styles from "./styles";

const DetailLabel = ({ title = "", text = "" }) => {
  return (
    <View style={styles.content}>
      <Title style={styles.title}>{title}</Title>
      <Text>{text}</Text>
    </View>
  );
};

export default DetailLabel;
