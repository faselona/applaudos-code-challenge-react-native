import { StyleSheet } from "react-native";

export default StyleSheet.create({
  content: {
    width: "100%",
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    textDecorationLine: "underline",
  },
});
