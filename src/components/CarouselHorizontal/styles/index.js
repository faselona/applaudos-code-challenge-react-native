import { StyleSheet } from "react-native";

export default StyleSheet.create({
  serieCard: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 1,
    shadowRadius: 10,
  },
  cardImage: {
    width: "100%",
    height: 250,
    borderRadius: 20,
  },
  titleCard: {
    marginHorizontal: 10,
    marginTop: 10,
    fontSize: 14,
    textAlign: "center",
  },
});
