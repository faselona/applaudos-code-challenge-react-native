import React from "react";
import {
  View,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from "react-native";
import { Title } from "react-native-paper";
import Carousel from "react-native-snap-carousel";
import { get } from "lodash";

import styles from "./styles";

const { width } = Dimensions.get("window");
const ITEM_WIDTH = Math.round(width * 0.4);

const CarouselHorizontal = ({ data = [], navigation }) => {
  return (
    <Carousel
      layout={"default"}
      data={data}
      renderItem={(item) => <RenderItem data={item} navigation={navigation} />}
      sliderWidth={width}
      itemWidth={ITEM_WIDTH}
    />
  );
};

export default CarouselHorizontal;

function RenderItem({ data, navigation }) {
  const { item } = data;
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        navigation.navigate("detail", { id: item.id, type: item.type })
      }
    >
      <View style={styles.serieCard}>
        <Image
          style={styles.cardImage}
          source={{ uri: get(item, "attributes.posterImage.small", "") }}
        />
        <Title style={styles.titleCard}>
          {get(item, "attributes.titles.en", "") || "Title not available"}
        </Title>
      </View>
    </TouchableWithoutFeedback>
  );
}
