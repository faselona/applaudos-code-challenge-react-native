import React from "react";
import { View, ActivityIndicator } from "react-native";

import styles from "./styles";

const Loading = () => {
  return (
    <View style={styles.loaderContent}>
      <ActivityIndicator size="large" color="#FFFFFF" />
    </View>
  );
};

export default Loading;
