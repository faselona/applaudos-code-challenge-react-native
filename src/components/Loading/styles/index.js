import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  loaderContent: {
    width: width,
    height: height,
    justifyContent: "center",
  },
});
