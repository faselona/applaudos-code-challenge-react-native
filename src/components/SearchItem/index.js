import React from "react";
import {
  TouchableWithoutFeedback,
  View,
  Image,
  StyleSheet,
  Dimensions,
} from "react-native";
import { get } from "lodash";

import styles from "./styles";

const SearchItem = ({ serie, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.serieItem}>
        <Image
          style={styles.image}
          source={{ uri: get(serie, "attributes.posterImage.small") }}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default SearchItem;
