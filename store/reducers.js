import { combineReducers } from "redux";

import series from "./series/reducers";

export default combineReducers({
  series,
});
