//Serie Detail
import {
  GET_SERIE_DETAIL_REQUEST,
  GET_SERIE_DETAIL_SUCCESS,
  GET_SERIE_DETAIL_ERROR,
} from "../../actions/serieDetail/getSerieDetail";

//Serie Genres
import {
  GET_SERIE_GENRES_REQUEST,
  GET_SERIE_GENRES_SUCCESS,
  GET_SERIE_GENRES_ERROR,
} from "../../actions/serieDetail/getSerieGenres";

const INITIAL_STATE = {
  loading: false,
  error: false,
  data: {},
  genres: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SERIE_DETAIL_REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GET_SERIE_DETAIL_SUCCESS:
      return {
        ...state,
        data: action.payload,
        error: false,
        loading: false,
      };
    case GET_SERIE_DETAIL_ERROR:
      return {
        ...state,
        error: action.payload || action.payload.status || 401,
        loading: false,
      };
    case GET_SERIE_GENRES_REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GET_SERIE_GENRES_SUCCESS:
      return {
        ...state,
        genres: action.payload,
        error: false,
        loading: false,
      };
    case GET_SERIE_GENRES_ERROR:
      return {
        ...state,
        error: action.payload || action.payload.status || 401,
        loading: false,
      };

    default:
      return state;
  }
};
