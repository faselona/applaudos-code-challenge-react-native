//All Search Series
import {
  GET_SEARCH_SERIES_REQUEST,
  GET_SEARCH_SERIES_SUCCESS,
  GET_SEARCH_SERIES_ERROR,
} from "../../actions/searchSeries/getSearchSeries";

const INITIAL_STATE = {
  loading: false,
  error: false,
  data: [],
  meta: {},
  links: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SEARCH_SERIES_REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GET_SEARCH_SERIES_SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case GET_SEARCH_SERIES_ERROR:
      return {
        ...state,
        error: action.payload || action.payload.status || 401,
        loading: false,
      };
    default:
      return state;
  }
};
