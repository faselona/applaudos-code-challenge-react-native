//All Anime
import {
  GET_ANIME_REQUEST,
  GET_ANIME_SUCCESS,
  GET_ANIME_ERROR,
} from "../../actions/anime/getAnime";

const INITIAL_STATE = {
  loading: false,
  error: false,
  data: [],
  meta: {},
  links: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ANIME_REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GET_ANIME_SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case GET_ANIME_ERROR:
      return {
        ...state,
        error: action.payload || action.payload.status || 401,
        loading: false,
      };
    default:
      return state;
  }
};
