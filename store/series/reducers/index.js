import { combineReducers } from "redux";

import anime from "./anime";
import manga from "./manga";
import searchSeries from "./searchSeries";
import serieDetail from "./serieDetail";

export default combineReducers({
  anime,
  manga,
  searchSeries,
  serieDetail,
});
