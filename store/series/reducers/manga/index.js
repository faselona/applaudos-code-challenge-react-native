//All Manga
import {
  GET_MANGA_REQUEST,
  GET_MANGA_SUCCESS,
  GET_MANGA_ERROR,
} from "../../actions/manga/getManga";

const INITIAL_STATE = {
  loading: false,
  error: false,
  data: [],
  meta: {},
  links: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_MANGA_REQUEST:
      return {
        ...state,
        error: false,
        loading: true,
      };
    case GET_MANGA_SUCCESS:
      return {
        ...state,
        ...action.payload,
        error: false,
        loading: false,
      };
    case GET_MANGA_ERROR:
      return {
        ...state,
        error: action.payload || action.payload.status || 401,
        loading: false,
      };
    default:
      return state;
  }
};
