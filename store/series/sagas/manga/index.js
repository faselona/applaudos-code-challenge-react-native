import { put, takeLatest, call } from "redux-saga/effects";
import { get } from "lodash";

//* All Manga
import {
  // action types
  GET_MANGA_REQUEST,
  // requests
  getMangaServiceRequest,
  // actions
  getMangaSuccess,
  getMangaError,
} from "../../actions/manga/getManga";

function* getManga(action) {
  try {
    const result = yield call(getMangaServiceRequest, action.payload);

    const data = get(result, "data", {});

    yield put(getMangaSuccess(data));

    action.callback && action.callback(null, data);
  } catch (e) {
    yield put(getMangaError(e.response));

    action.callback && action.callback(e.response);
  }
}

function* mangaSaga() {
  yield takeLatest(GET_MANGA_REQUEST, getManga);
}

export default mangaSaga;
