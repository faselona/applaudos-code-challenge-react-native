import anime from "./anime";
import manga from "./manga";
import searchSeries from "./searchSeries";
import serieDetail from "./serieDetail";

export default [anime, manga, searchSeries, serieDetail];
