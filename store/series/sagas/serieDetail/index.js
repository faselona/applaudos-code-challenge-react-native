import { put, takeLatest, call } from "redux-saga/effects";
import { get } from "lodash";

//* Serie Detail
import {
  // action types
  GET_SERIE_DETAIL_REQUEST,
  // requests
  getSerieDetailServiceRequest,
  // actions
  getSerieDetailSuccess,
  getSerieDetailError,
} from "../../actions/serieDetail/getSerieDetail";

//* Serie Genres
import {
  // action types
  GET_SERIE_GENRES_REQUEST,
  // requests
  getSerieGenresServiceRequest,
  // actions
  getSerieGenresSuccess,
  getSerieGenresError,
} from "../../actions/serieDetail/getSerieGenres";

function* getSerieDetail(action) {
  try {
    const result = yield call(getSerieDetailServiceRequest, action.payload);
    const data = get(result, "data.data", {});

    yield put(getSerieDetailSuccess(data));

    action.callback && action.callback(null, data);
  } catch (e) {
    yield put(getSerieDetailError(e.response));

    action.callback && action.callback(e.response);
  }
}
function* getSerieGenres(action) {
  try {
    const result = yield call(getSerieGenresServiceRequest, action.payload);
    const data = get(result, "data.data", {});

    yield put(getSerieGenresSuccess(data));

    action.callback && action.callback(null, data);
  } catch (e) {
    yield put(getSerieGenresError(e.response));

    action.callback && action.callback(e.response);
  }
}

function* serieDetailSaga() {
  yield takeLatest(GET_SERIE_DETAIL_REQUEST, getSerieDetail);
  yield takeLatest(GET_SERIE_GENRES_REQUEST, getSerieGenres);
}

export default serieDetailSaga;
