import { put, takeLatest, call } from "redux-saga/effects";
import { get } from "lodash";

//* All Search Series
import {
  // action types
  GET_SEARCH_SERIES_REQUEST,
  // requests
  getSearchSeriesServiceRequest,
  // actions
  getSearchSeriesSuccess,
  getSearchSeriesError,
} from "../../actions/searchSeries/getSearchSeries";

function* getSearchSeries(action) {
  try {
    const result = yield call(getSearchSeriesServiceRequest, action.payload);

    const data = get(result, "data", {});

    yield put(getSearchSeriesSuccess(data));

    action.callback && action.callback(null, data);
  } catch (e) {
    yield put(getSearchSeriesError(e.response));

    action.callback && action.callback(e.response);
  }
}

function* searchSeriesSaga() {
  yield takeLatest(GET_SEARCH_SERIES_REQUEST, getSearchSeries);
}

export default searchSeriesSaga;
