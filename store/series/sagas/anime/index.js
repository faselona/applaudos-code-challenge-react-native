import { put, takeLatest, call } from "redux-saga/effects";
import { get } from "lodash";

//* All Anime
import {
  // action types
  GET_ANIME_REQUEST,
  // requests
  getAnimeServiceRequest,
  // actions
  getAnimeSuccess,
  getAnimeError,
} from "../../actions/anime/getAnime";

function* getAnime(action) {
  try {
    const result = yield call(getAnimeServiceRequest, action.payload);

    const data = get(result, "data", {});

    yield put(getAnimeSuccess(data));

    action.callback && action.callback(null, data);
  } catch (e) {
    yield put(getAnimeError(e.response));

    action.callback && action.callback(e.response);
  }
}

function* animeSaga() {
  yield takeLatest(GET_ANIME_REQUEST, getAnime);
}

export default animeSaga;
