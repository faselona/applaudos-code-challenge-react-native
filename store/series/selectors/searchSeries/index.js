import { createSelector } from "reselect";

const searchSeries = (state) => state.series.searchSeries;

export const loading = createSelector(searchSeries, (state) => state.loading);

export const data = createSelector(searchSeries, (state) => state.data);
