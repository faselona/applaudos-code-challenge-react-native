import { createSelector } from "reselect";

const manga = (state) => state.series.manga;

export const loading = createSelector(manga, (state) => state.loading);

export const data = createSelector(manga, (state) => state.data);
