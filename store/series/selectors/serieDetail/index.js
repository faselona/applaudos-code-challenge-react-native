import { createSelector } from "reselect";

const serieDetail = (state) => state.series.serieDetail;

export const loading = createSelector(serieDetail, (state) => state.loading);

export const data = createSelector(serieDetail, (state) => state.data);

export const genres = createSelector(serieDetail, (state) => state.genres);
