import { createSelector } from "reselect";

const anime = (state) => state.series.anime;

export const loading = createSelector(anime, (state) => state.loading);

export const data = createSelector(anime, (state) => state.data);
