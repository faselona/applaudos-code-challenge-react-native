import request, { Methods } from "../../../../src/utils/request";

//* ACTIONTYPES --------------------------------------------
export const GET_SERIE_DETAIL_REQUEST = "GET_SERIE_DETAIL_REQUEST";
export const GET_SERIE_DETAIL_SUCCESS = "GET_SERIE_DETAIL_SUCCESS";
export const GET_SERIE_DETAIL_ERROR = "GET_SERIE_DETAIL_ERROR";

//* ACTIONS ------------------------------------------------
export const getSerieDetailRequest = (payload = {}, callback) => ({
  type: GET_SERIE_DETAIL_REQUEST,
  payload,
  callback,
});

export const getSerieDetailSuccess = (data) => ({
  type: GET_SERIE_DETAIL_SUCCESS,
  payload: data,
});

export const getSerieDetailError = (error) => ({
  type: GET_SERIE_DETAIL_ERROR,
  payload: error,
});

//* REQUEST SERVICE ---------------------------------------
export const getSerieDetailServiceRequest = async ({ serieId, serieType }) => {
  const options = {
    method: Methods.GET,
  };

  return request(`/${serieType}/${serieId}`, options);
};
