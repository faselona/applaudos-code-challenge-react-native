import request, { Methods } from "../../../../src/utils/request";

//* ACTIONTYPES --------------------------------------------
export const GET_SERIE_GENRES_REQUEST = "GET_SERIE_GENRES_REQUEST";
export const GET_SERIE_GENRES_SUCCESS = "GET_SERIE_GENRES_SUCCESS";
export const GET_SERIE_GENRES_ERROR = "GET_SERIE_GENRES_ERROR";

//* ACTIONS ------------------------------------------------
export const getSerieGenresRequest = (payload = {}, callback) => ({
  type: GET_SERIE_GENRES_REQUEST,
  payload,
  callback,
});

export const getSerieGenresSuccess = (data) => ({
  type: GET_SERIE_GENRES_SUCCESS,
  payload: data,
});

export const getSerieGenresError = (error) => ({
  type: GET_SERIE_GENRES_ERROR,
  payload: error,
});

//* REQUEST SERVICE ---------------------------------------
export const getSerieGenresServiceRequest = async ({ serieId, serieType }) => {
  const options = {
    method: Methods.GET,
  };

  return request(`/${serieType}/${serieId}/genres`, options);
};
