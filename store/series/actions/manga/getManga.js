import request, { Methods } from "../../../../src/utils/request";

//* ACTIONTYPES --------------------------------------------
export const GET_MANGA_REQUEST = "GET_MANGA_REQUEST";
export const GET_MANGA_SUCCESS = "GET_MANGA_SUCCESS";
export const GET_MANGA_ERROR = "GET_MANGA_ERROR";

//* ACTIONS ------------------------------------------------
export const getMangaRequest = (payload = {}, callback) => ({
  type: GET_MANGA_REQUEST,
  payload,
  callback,
});

export const getMangaSuccess = (data) => ({
  type: GET_MANGA_SUCCESS,
  payload: data,
});

export const getMangaError = (error) => ({
  type: GET_MANGA_ERROR,
  payload: error,
});

//* REQUEST SERVICE ---------------------------------------
export const getMangaServiceRequest = async (params) => {
  const options = {
    method: Methods.GET,
    params: {
      ...params,
      page: {
        limit: 20,
        offset: 0,
      },
    },
  };

  return request(`/manga`, options);
};
