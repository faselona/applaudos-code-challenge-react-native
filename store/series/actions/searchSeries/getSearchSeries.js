import request, { Methods } from "../../../../src/utils/request";

//* ACTIONTYPES --------------------------------------------
export const GET_SEARCH_SERIES_REQUEST = "GET_SEARCH_SERIES_REQUEST";
export const GET_SEARCH_SERIES_SUCCESS = "GET_SEARCH_SERIES_SUCCESS";
export const GET_SEARCH_SERIES_ERROR = "GET_SEARCH_SERIES_ERROR";

//* ACTIONS ------------------------------------------------
export const getSearchSeriesRequest = (payload = {}, callback) => ({
  type: GET_SEARCH_SERIES_REQUEST,
  payload,
  callback,
});

export const getSearchSeriesSuccess = (data) => ({
  type: GET_SEARCH_SERIES_SUCCESS,
  payload: data,
});

export const getSearchSeriesError = (error) => ({
  type: GET_SEARCH_SERIES_ERROR,
  payload: error,
});

//* REQUEST SERVICE ---------------------------------------
export const getSearchSeriesServiceRequest = async (text) => {
  const options = {
    method: Methods.GET,
    params: {
      filter: {
        text,
      },
    },
  };

  return request(`/anime`, options);
};
