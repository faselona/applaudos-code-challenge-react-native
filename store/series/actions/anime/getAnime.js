import request, { Methods } from "../../../../src/utils/request";

//* ACTIONTYPES --------------------------------------------
export const GET_ANIME_REQUEST = "GET_ANIME_REQUEST";
export const GET_ANIME_SUCCESS = "GET_ANIME_SUCCESS";
export const GET_ANIME_ERROR = "GET_ANIME_ERROR";

//* ACTIONS ------------------------------------------------
export const getAnimeRequest = (payload = {}, callback) => ({
  type: GET_ANIME_REQUEST,
  payload,
  callback,
});

export const getAnimeSuccess = (data) => ({
  type: GET_ANIME_SUCCESS,
  payload: data,
});

export const getAnimeError = (error) => ({
  type: GET_ANIME_ERROR,
  payload: error,
});

//* REQUEST SERVICE ---------------------------------------
export const getAnimeServiceRequest = async (params) => {
  const options = {
    method: Methods.GET,
    params: {
      ...params,
      page: {
        limit: 20,
        offset: 0,
      },
    },
  };

  return request(`/anime`, options);
};
